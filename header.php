<?php status_header(200); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">

  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title>The PetSafe® Bark for Your Park™ Grant Contest</title>
    <link rel="icon" href="<?php the_field('favicon', 'options') ?>" type="image/x-icon" />
    <?php wp_head(); ?>
  </head>

  <body>
    <?php wp_body_open(); ?>
