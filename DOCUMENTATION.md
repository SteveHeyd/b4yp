# Bark for Your Park Site Documentation

**Repo**: [https://bitbucket.org/SteveHeyd/b4yp/src/main/](https://bitbucket.org/SteveHeyd/b4yp/src/main/)

**Built on**: [VueJS](https://vuejs.org/), [Tailwind](https://v2.tailwindcss.com/docs), and [Wordpress](https://codex.wordpress.org/)

Reach out to [steve@highdwellercreative.com](mailto:steve@highdwellercreative.com) with any questions.


## Development


#### UI Development

**Project setup**


```
yarn install
```


**Compiles and hot-reloads for development**

This will serve the site for development against either a locally running WP instance or one of the hosted API origins. You can determine which is used by setting `VUE_APP_API_ORIGIN`. Default is to use production instance. Recommendation is to set this to local, dev or staging via a .env file.


```
yarn serve
```


**Compiles and minifies**

`yarn build` - use for local development against locally running WP instance

**Lints and fixes files**


```
yarn lint
```


**UI API Username**: application_submitter - _Set in .env_

**UI API Password**: set new password [here](https://b4ypdev.wpengine.com/wp-admin/user-edit.php?user_id=7#application-passwords-section) each time a new person or server needs access. Link is to the dev site but the same path works for stage and production, just update the origin. Set password in .env.local


#### API Development

Custom API endpoints are defined in `./lib/custom-endpoints.php`. The integration with Bronto is separated into `./lib/bronto.class.php`. 

The UI accesses the API via the store in various files under `./src/store/modules/`. These files provide getters, setters and mutators that are used within the application to manage state and process data from the API. 


#### Application Handling

The code to handle application processing is located in `./lib/application-handling.php`.

**update_application() - _tied to save hook on application post type - This is the primary function for handling applications._**

This function handles setting up the application as a finalist and creating a location post marked as a finalist and populated with details from the application.

**display_application_save_errors()**

This function handles displaying any errors encountered when saving an application via the admin_notices hook and utilizes transient data to persist the errors until displayed.

**delete_location_post()** - _tied to delete hook on location post type_

This function handles reverting an application previously marked as a finalist when it’s location post is deleted.


#### Custom Wordpress Functionality

Custom styles for the TinyMCE editors are defined in `./lib/tiny-mce-styles.php`. The TinyMCE editor is also set to give links relative URLs instead of absolute URLs for page/post links to enable smooth linking in the UI. The WP link modal is also modified to return relative URLs by striping out the base url via the `wp_link_query` filter. 


#### Custom Wordpress Admin Views

There are three custom views used to display the applications, finalist location application details, and the vote results. These are `./vote-admin-view.php`, `./application-admin-view.php`, and `./location-admin-view.php`.


## Deployment

Use the environment specific build command from below to build the deployable assets for the site. Once the site is built, upload the `./dist` folder to the `./wp-content/themes/b4yp/` directory on the  appropriate server. Be sure to also upload any modified PHP files. Be sure to avoid uploading anything other than modified php files and the built dist directory to ensure the server holds only what it needs to display and power the site.


#### Build Commands

`yarn build-dev` - sets the API Origin to b4ypdev.wpengine.com before build. Use to  deploy to development site in WP Engine

`yarn build-stg` - sets the API Origin to b4ypstage.wpengine.com before build. Use to deploy to staging site in WP Engine

`yarn build-prod` - sets the API Origin to barkforyourpark.petsafe.com before build. Use to deploy to production site in WP Engine (or anywhere is is hosted)


## General Information

The primary files for the frontend development live under `./src`. There are five primary page templates: FourOhFour, Home, Location, Page, and Post.


### FourOhFour

Simple template that displays whenever a page or post is not found.


### Home

This template displays to primary landing page of the site. It utilizes a loop to display different components as defined in the Wordpress backend.


### Location

This is a specialty template for displaying a location post type. The top half of the page displays the location title, the voting mechanism/status, and the featured image. The bottom half of the page is a loop of components defined in Wordpress.


### Page

This is the primary template for displaying pages. This template has three variations, controlled via settings in Wordpress. All variations have a loop of components at the bottom for customization.



1. Location Map
    1. Displays the map of locations at the the top, followed by a sorted/searchable list. The map on this page is built with [gmap-vue](https://github.com/diegoazh/gmap-vue#readme)
2. Blog Index
    2. This displays if the page ID matches the posts page ID from the WordPress settings
3. Standard Page
    3. Displays Hero at top of page


### Post

Specialty template to display individual blog posts.
