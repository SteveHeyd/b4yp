**Project setup**

`yarn install`

**Compiles and hot-reloads for development**

This will serve the site for development against either a locally running WP instance or one of the hosted API origins. You can determine which is used by setting `VUE_APP_API_ORIGIN`. Default is to use production instance. Recommendation is to set this to local, dev or staging via a .env file.

`yarn serve`

**Compiles and minifies**

`yarn build` - use for local development against locally running WP instance

`yarn build-dev` - sets the API Origin to b4ypdev.wpengine.com before build. Use to  deploy to development site in WP Engine

`yarn build-stg` - sets the API Origin to b4ypstage.wpengine.com before build. Use to deploy to staging site in WP Engine

`yarn build-prod` - sets the API Origin to barkforyourpark.petsafe.com before build. Use to deploy to production site in WP Engine (or anywhere is is hosted)

**Lints and fixes files**

`yarn lint`