<?php
use \Verifalia\VerifaliaRestClient;

define('BRONTO_LIST_ID__VOTING_OPTED_IN', '0be203ec00000000000000000000001bd493');
define('BRONTO_LIST_ID__VOTING_OPTED_OUT', '0be203ec00000000000000000000001bd494');
define('BRONTO_LIST_ID__GENERAL_MARKETING', '0be203ec00000000000000000000001b3c53');

add_action( 'rest_api_init', function () {
  register_rest_route( 'b4yp/v1', '/settings', array(
    'methods' => 'GET',
    'callback' => 'hdc_get_settings',
    'permission_callback' => '__return_true',
  ) );
  register_rest_route( 'b4yp/v1', '/submit-email', array(
    'methods' => 'POST',
    'callback' => 'b4yp_submit_email_to_bronto',
    'permission_callback' => function () {
      return current_user_can( 'publish_b4yp_applications' );
    }
  ) );
  register_rest_route( 'b4yp/v1', '/submit-application', array(
    'methods' => 'POST',
    'callback' => 'b4yp_submit_application',
    'permission_callback' => function () {
      return current_user_can( 'publish_b4yp_applications' );
    }
  ) );
  register_rest_route( 'b4yp/v1', '/submit-vote', array(
    'methods' => 'POST',
    'callback' => 'b4yp_submit_vote',
    'permission_callback' => function () {
      return current_user_can( 'publish_b4yp_votes' );
    }
  ) );
});

function get_nav_items_for_site($name) {
	$menu = wp_get_nav_menu_items($name);
	$menuItems = array_map(function($item) {
		$postObject = get_post($item->object_id);
		$type = $item->object;
		$name = $type === 'custom' ? $item->url : "/$postObject->post_name";
		return [
			"type" => $type,
			"item" => $item,
			"label" => $item->title,
			"name" => $name,
			"title" => $item->attr_title
		];
	}, $menu);
	return $menuItems;
}

function hdc_get_settings() {
  $headerMenuName = 'Main Menu';
	$headerMenuObject = wp_get_nav_menu_object($headerMenuName);
	$footerMenuName = 'Footer Menu';
	$footerMenuObject = wp_get_nav_menu_object($footerMenuName);
	$response = [];
	$options = get_fields('options');
	$response['header'] = [
		"announcement" => [
			"display" => array_key_exists('announcement_message', $options) ? $options['display_announcement'] : false,
			"message" => array_key_exists('announcement_message', $options) ? $options['announcement_message'] : null,
		],
    "menu" => [
			"logo" => get_field('logo', $headerMenuObject),
			"call-to-action" => get_field('call_to_action', $headerMenuObject),
			"items" => get_nav_items_for_site($headerMenuName),
		],
	];
	$response['footer'] = [
		"menu" => [
			"items" => get_nav_items_for_site($footerMenuName),
			"call-to-action" => get_field('footer_call_to_action', $footerMenuObject),
		],
	];
  $response['posts-page'] = get_option( 'page_for_posts' );
	return $response;
}

include "bronto.class.php";

function send_email_to_bronto($email, $list_id) {
  $bronto = new Bronto;
  $bronto->login();
  $bronto->add_contact($email);
  $bronto->add_contact_to_list($list_id); // BFYP 2021 list
  return $bronto;
}

function b4yp_submit_email_to_bronto($request) {
	$body = $request->get_json_params();
	$email = $body['email'];
	if (!is_email($email)) {
		return ['success' => false, 'message' => 'That was not a valid email.'];
	}
	try {
		$result = send_email_to_bronto($email, BRONTO_LIST_ID__GENERAL_MARKETING);
		if ($result->was_existing_contact) {
			return ['success' => false, 'message' => 'That email was already submitted'];
		}
	} catch (Exception $error) {
		return ['success' => false, 'message' => $error->getMessage()];
	}
	return $response = ['success' => true, 'message' => 'Email submitted'];
}

function b4yp_submit_application($request) {
  $post_type = 'b4yp_applications';
	$body = $request->get_json_params();
  $fullname = $body['firstName'] . ' ' . $body['lastName'];
  $address = $body['address'];
  $name = $body['dogParkType'] === 'New Park' ? $body['parkName'] : $body['communityName'];
  $title = "{$name} - {$address['city']}, {$address['state']} - {$fullname}";
  $args = [
    'post_type' => $post_type,
    'post_title' => $title,
    'meta_input' => $body,
  ];
  $post_id = wp_insert_post($args, true);
  if (is_wp_error($post_id)) {
    write_error('FORM ERROR ++++++++++++++++++++');
    write_error($post_id);
    return [ 'success' => false, 'message' => 'There was an issue on the server. Please do not refresh or close this page and try resubmitting in a few minutes.' ];
  } else {
    return [ 'success' => true ];
  }
}

function replaceBetween($string, $needleStart, $needleEnd, $replacement,
                        $replaceNeedles = true, $startPos = 0) {
    $posStart = mb_strpos($string, $needleStart, $startPos);

    if ($posStart === false) {
        return $string;
    }

    $start = $posStart + ($replaceNeedles ? 0 : mb_strlen($needleStart));
    $posEnd = mb_strpos($string, $needleEnd, $start);

    if ($posEnd === false) {
        return $string;
    }

    $length = $posEnd - $start + ($replaceNeedles ? mb_strlen($needleEnd) : 0);

    $result = substr_replace($string, $replacement, $start, $length);

    return $result;
}

function is_verified_email($email) {
  if (!is_email($email)) {
		return ['success' => false, 'message' => 'That was not a valid email.'];
	}
  $verifalia = new VerifaliaRestClient([
    'username' => '260e259c3bc14624a1e4bb66fb44bf08',
    'password' => 'TFR-dwu*wqt1wuc.xme'
  ]);
  try {
    $validation = $verifalia
      ->emailValidations
      ->submit($email, true);
    $job_id = $validation->overview->id;
    try {
      $verifalia
        ->emailValidations
        ->delete($job_id);
    } catch (Exception $error) {
      //
    }
    $entry = $validation->entries[0];
    $is_deliverable = $entry->classification === 'Deliverable' && $entry->status === 'Success';
    if ($is_deliverable) {
      return true;
    } else {
      return ['success' => false, 'message' => 'Oops! We couldn\'t verify that the email you entered exists. Please double-check the entry and try again. If using a work email, try a personal one.'];
    }
  } catch (Exception $error) {
    write_error($error);
    return true; // fail open
  }
}

function b4yp_submit_vote($request) {
  $post_type = 'b4yp_votes';
	$body = $request->get_json_params();
  $email = $body['email'];
  $did_opt_in_to_marketing = $body['marketing_opt_in'];
  $location_id = $body['location_id'];
  $base_email = html_entity_decode( replaceBetween($email, '+', '@', '@') );
  $email_post = get_page_by_title($base_email, 'OBJECT', $post_type);
  $today = date("Y/m/d");

  $email_verified = is_verified_email($base_email);
  if ($email_verified !== true) {
		return $email_verified;
	}

  $list_id = $did_opt_in_to_marketing !== 'no' ? BRONTO_LIST_ID__VOTING_OPTED_IN : BRONTO_LIST_ID__VOTING_OPTED_OUT;
  try {
    send_email_to_bronto($base_email, $list_id);
  } catch (Exception $error) {
    // do nothing
  }

  if (is_null($email_post)) {
    $args = [
      'post_type' => $post_type,
      'post_title' => $base_email,
      'meta_input' => [
        'voted_for' => $location_id,
        'last_voted_on' => $today,
      ],
    ];
    $post_id = wp_insert_post($args, true);

    if (is_wp_error($post_id)) {
      // log errors
      write_error('VOTE ERROR ++++++++++++++++++++');
      write_error('Email Insert Error ++');
      write_error($post_id);

      return [ 'success' => false, 'message' => 'There was an issue on the server. Try again in a few minutes.' ];
    } else {
      $vote_received = add_post_meta($location_id, 'vote_received', $post_id);

      if ($vote_received) {
        return [ 'success' => true ];
      } else {
        // log errors
        write_error('VOTE ERROR ++++++++++++++++++++');
        write_error("Vote Received errored after inserting email");

        return [ 'success' => false, 'message' => 'There was an issue on the server. Try again in a few minutes.'];
      }
    }
  } else {
    $email_post_id = $email_post->ID;
    $last_voted_on = get_post_meta($email_post_id, 'last_voted_on', true);

    if ($last_voted_on == $today) {
      $time = date('Y/m/d H:m:i T');
      // log errors
      write_error('VOTE ERROR ++++++++++++++++++++');
      write_error("Voting too many times in one day. Base email: {$base_email} -- Attempted with: {$email}");
      write_error("Last voted on: {$last_voted_on} - Today: {$today}");

      // log this error on the email post for tracking
      add_post_meta($email_post_id, 'vote_not_allowed', $time);
      if ($base_email != $email) {
        add_post_meta($email_post_id, 'vote_not_allowed_plus_sign', true);
      }
      return [ 'success' => false, 'message' => 'You\'ve already voted today. Come back and vote tomorrow!' ];
    } else {
      $vote_received = add_post_meta($location_id, 'vote_received', $email_post_id);
      $vote_for = add_post_meta($email_post_id, 'voted_for', $location_id);
      $last_voted_on_update = update_post_meta($email_post_id, 'last_voted_on', $today);

      if ($vote_received && $vote_for && $last_voted_on_update) {
        return [ 'success' => true ];
      } else {
        // log errors
        write_error('VOTE ERROR ++++++++++++++++++++');
        write_error("Vote Received Error? {$vote_received} (Is there a number here)");
        write_error("Vote For Error? {$vote_for} (Is there a number here)");
        write_error("Last Voted On Error? {$last_voted_on_update} (Is there a number here)");

        return [ 'success' => false, 'message' => 'There was an issue on the server. Try again in a few minutes.'];
      }
    }
  }
}
