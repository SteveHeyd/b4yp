<?php
// add_action('init', 'lock_down_site');
date_default_timezone_set('America/New_York');

function lock_down_site() {
    // WP tracks the current page - global the variable to access it
    global $pagenow;
    // Check if a $_GET['action'] is set, and if so, load it into $action variable
    $action = (isset($_GET['action'])) ? $_GET['action'] : '';
    // Check if we're on the login page, and ensure the action is not 'logout'
    if (
      (
        !is_admin() &&
        !is_user_logged_in()&&
        !defined('REST_REQUEST') &&
        $pagenow != 'wp-login.php' ||
        ($_GET['install_name'] && $_GET['install_name'] != 'b4ypdev')
      )
     ) {
       wp_redirect( 'https://barkforyourpark.petsafe.com' . $_SERVER['REQUEST_URI'], 301 );
       exit;
    }
}

function write_error($error) {
  ob_start();
  print_r($error);
  error_log(ob_get_clean());
}

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/custom-endpoints.php';
require_once __DIR__ . '/lib/application-handling.php';
require_once __DIR__ . '/lib/tiny-mce-styles.php';

function get_acf_group( $slug ) {
  $query = new WP_Query(
    [
      'name'   => $slug,
      'post_type'   => 'acf-field-group',
      'numberposts' => 1,
    ]
  );
  $posts = $query->get_posts();
  return array_shift( $posts );
}

add_filter( 'wp_is_application_passwords_available', '__return_true' );

// Remove all default WP template redirects/lookups
remove_action( 'template_redirect', 'redirect_canonical' );

add_theme_support('menus');
add_theme_support('post-thumbnails');

// Redirect all requests to index.php so the Vue app is loaded and 404s aren't thrown
function remove_redirects() {
	add_rewrite_rule( '^/(.+)/?', 'index.php', 'top' );
}
add_action( 'init', 'remove_redirects' );

// Locations Post Type
function b4yp_locations_custom_post_type() {
  register_post_type('b4yp_locations',
    [
      'labels'      => [
        'name'          => __('Locations', 'b4yp'),
        'singular_name' => __('Location', 'b4yp'),
      ],
      'public'        => true,
      'has_archive'   => true,
      'show_in_rest'  => true,
      'rest_base'     => 'locations',
      'rewrite'       => [
        'slug'          => 'locations',
        'with_front'    => false,
      ],
      'supports'      => ['thumbnail', 'title', 'content']
    ]
  );
}
add_action('init', 'b4yp_locations_custom_post_type');

function b4yp_votes_custom_post_type() {
  register_post_type('b4yp_votes',
    [
      'labels'      => [
        'name'          => __('Votes', 'b4yp'),
        'singular_name' => __('Vote', 'b4yp'),
      ],
      'public'        => true,
      'show_in_menu'  => true,
      'has_archive'   => false,
      'show_in_rest'  => false,
      'supports'      => ['title'],
      'capability_type'     => array('b4yp_vote','b4yp_votes'),
      'map_meta_cap'        => true,
    ]
  );
}
add_action('init', 'b4yp_votes_custom_post_type');

// Applications Post Type
function b4yp_applications_custom_post_type() {
  register_post_type('b4yp_applications',
    [
      'labels'      => [
        'name'          => __('Applications', 'b4yp'),
        'singular_name' => __('Application', 'b4yp'),
      ],
      'public'        => true,
      'has_archive'   => true,
      'show_in_rest'  => true,
      'show_in_menu'  => true,
      'rest_base'     => 'applications',
      'rewrite'       => [
        'slug'          => 'applications',
        'with_front'    => false,
      ],
      'supports'            => [],
      'capability_type'     => array('b4yp_application','b4yp_applications'),
      'map_meta_cap'        => true,
    ]
  );
}
add_action('init', 'b4yp_applications_custom_post_type');

define('GMAP_API_KEY', 'AIzaSyD7zVOgZJuzV7QU_DDSsCE95WISkECGl8c');
function add_gmaps_key_to_acf() {
	acf_update_setting('google_api_key', GMAP_API_KEY);
}

add_action('acf/init', 'add_gmaps_key_to_acf');


// Load scripts
function load_vue_scripts() {
	$js = [
		'b4yp-vendors' => '/dist/js/chunk-vendors.js',
		'b4yp-app' => '/dist/js/app.js',
	];
	foreach ($js as $name => $file) {
		wp_enqueue_script(
			$name,
			get_stylesheet_directory_uri() . $file,
			filemtime( get_stylesheet_directory() . $file ),
			null,
			true
		);
	}

  $css = [
		'b4yp-vendors-css' => '/dist/css/chunk-vendors.css',
		'b4yp-app-css' => '/dist/css/app.css',
	];
	foreach ($css as $name => $file) {
    wp_enqueue_style(
      $name,
      get_stylesheet_directory_uri()  . $file,
      null,
      filemtime( get_stylesheet_directory()  . $file )
    );
	}

	wp_enqueue_style(
		'b4yp-css',
		get_stylesheet_directory_uri() . '/dist/css/app.css',
		null,
		filemtime( get_stylesheet_directory() . '/dist/css/app.css' )
	);
}
add_action( 'wp_enqueue_scripts', 'load_vue_scripts', 100 );

function acf_to_rest_api($response, $post, $request) {
	if (!function_exists('get_fields')) return $response;

	if (isset($post)) {
			$acf = [];
			$fields = get_field_objects($post->id);
			foreach ($fields as $name => $data) {
				$field_group = get_post($data['parent']);
				$key = str_replace(' ', '_', strtolower($field_group->post_title));
				$acf[$key][$name] = $data['value'];
			}
			$response->data['acf'] = $acf;
	}
	return $response;
}
add_filter('rest_prepare_b4yp_locations', 'acf_to_rest_api', 10, 3);
add_filter('rest_prepare_post', 'acf_to_rest_api', 10, 3);
add_filter('rest_prepare_page', 'acf_to_rest_api', 10, 3);

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'Settings',
		'menu_slug' 	=> 'general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

add_action('admin_init','b4yp_add_role_caps',999);
function b4yp_add_role_caps() {
  $appSub = get_role('application_submitter');
  if (is_null( $appSub )) {
    add_role( 'application_submitter', 'Application Submitter', [
      'read_b4yp_application' => true,
      'publish_b4yp_applications' => true,
      'upload_files' => true,
      'delete_post' => true,
    ] );
  }
  $appSub->add_cap( 'read_b4yp_vote');
  $appSub->add_cap( 'read_private_b4yp_votes' );
  $appSub->add_cap( 'edit_b4yp_vote' );
  $appSub->add_cap( 'edit_b4yp_votes' );
  $appSub->add_cap( 'edit_others_b4yp_votes' );
  $appSub->add_cap( 'edit_published_b4yp_votes' );
  $appSub->add_cap( 'publish_b4yp_votes' );
  $appSub->add_cap( 'delete_others_b4yp_votes' );
  $appSub->add_cap( 'delete_private_b4yp_votes' );
  $appSub->add_cap( 'delete_published_b4yp_votes' );

  if (!$appSub->has_cap('delete_posts')) {
    $appSub->add_cap('delete_posts');
  }
  if (is_null( get_role( 'developer' ) )) {
    add_role( 'developer', 'Developer', get_role( 'administrator' )->capabilities );
  }

  $roles = array('developer', 'editor', 'administrator');

  // Loop through each role and assign capabilities
  foreach($roles as $the_role) {
    $role = get_role($the_role);

    $role->add_cap( 'read' );
    $role->add_cap( 'read_b4yp_application');
    $role->add_cap( 'read_private_b4yp_applications' );
    $role->add_cap( 'edit_b4yp_application' );
    $role->add_cap( 'edit_b4yp_applications' );
    $role->add_cap( 'edit_others_b4yp_applications' );
    $role->add_cap( 'edit_published_b4yp_applications' );
    $role->add_cap( 'publish_b4yp_applications' );
    $role->add_cap( 'delete_others_b4yp_applications' );
    $role->add_cap( 'delete_private_b4yp_applications' );
    $role->add_cap( 'delete_published_b4yp_applications' );
    $role->add_cap( 'read_b4yp_vote');
    $role->add_cap( 'read_private_b4yp_votes' );
    $role->add_cap( 'edit_b4yp_vote' );
    $role->add_cap( 'edit_b4yp_votes' );
    $role->add_cap( 'edit_others_b4yp_votes' );
    $role->add_cap( 'edit_published_b4yp_votes' );
    $role->add_cap( 'publish_b4yp_votes' );
    $role->add_cap( 'delete_others_b4yp_votes' );
    $role->add_cap( 'delete_private_b4yp_votes' );
    $role->add_cap( 'delete_published_b4yp_votes' );
  }
}

function is_developer() {
  global $current_user;

  $user_roles = $current_user->roles;
  $role = array_shift($user_roles);

  return $role == 'developer';
}

function is_not_developer() {
  return !is_developer();
}

add_action( 'admin_menu', function () {
  if (is_not_developer()) remove_submenu_page( 'edit.php?post_type=page', 'post-new.php?post_type=page' );
  if (is_not_developer()) remove_submenu_page( 'edit.php?post_type=b4yp_applications', 'post-new.php?post_type=b4yp_applications' );
  remove_submenu_page( 'edit.php?post_type=b4yp_votes', 'post-new.php?post_type=b4yp_votes' );
});

add_action( 'admin_bar_menu', function (WP_Admin_Bar $admin_bar) {
  if (is_not_developer()) $admin_bar->remove_node('new-page');
  $admin_bar->remove_node('new-b4yp_applications');
  $admin_bar->remove_node('new-b4yp_votes');
}, 999);

add_action( 'current_screen', function () {
  global $pagenow;

  $currentScreen = get_current_screen();

  $block_for_all = $currentScreen->post_type === 'b4yp_applications' || $currentScreen->post_type === 'b4yp_votes';
  $is_page = $currentScreen->post_type === 'page';

  if (is_developer() && !$block_for_all) return;

  if ($block_for_all || $is_page) :
  ?>
    <style type="text/css">
      .page-title-action { display:none; }
    </style>
    <script>
      window.addEventListener('load', function () {
        document.querySelector('.page-title-action').remove();
      });
    </script>
  <?php
  endif;
});

add_action( 'admin_init', function () {
  global $pagenow;

  $post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : null;

  $block_for_all = $post_type === 'b4yp_applications' || $post_type === 'b4yp_votes';
  $is_page = $post_type === 'page';

  $votes_cache = [];

  if ($post_type === 'b4yp_votes' && $_GET['download']) {
    $applications = get_posts([
      'post_type' => 'b4yp_applications',
      'post_status' => 'any',
      'posts_per_page' => -1,
      'meta_key' => 'is_finalist',
      'meta_compare' => 'EXISTS',
    ]);
    $vote_data = [];
    foreach ($applications as $application) {
      $location_id = get_post_meta($application->ID, 'location_id', true);
      $votes = get_posts([
        'post_type' => 'b4yp_votes',
        'post_status' => 'any',
        'posts_per_page' => -1,
        'meta_key' => 'voted_for',
        'meta_value' => $location_id,
      ]);
      foreach ($votes as $vote) {
        $vote_id = $vote->ID;
        if (array_key_exists($vote_id, $votes_cache)) {
          $votes = $votes_cache[$vote_id];
        } else {
          $votes = get_post_meta($vote_id, 'voted_for');
          $votes_cache[$vote_id] = $votes;
        }
        $votes_for_location = array_filter($votes, function ($vote) use ($location_id) {
          return $vote === strval($location_id);
        });
        array_push($vote_data, [
          'location_name' => get_the_title($location_id),
          'email' => get_the_title($vote_id),
          'number_of_votes' => count($votes_for_location),
          'location_admin_url'=> get_edit_post_link($location_id),
          'first_voted_at' => get_the_date('Y-m-d H:i:s', $vote_id),
        ]);
      }
    }

    exit(export_csv($vote_data));
  }

  if (is_developer() && !$block_for_all) return;

  if (
    $pagenow == 'post-new.php' &&
    ( $block_for_all || $is_page )
  ) {
    wp_redirect( admin_url( "/edit.php?post_type={$post_type}" ) );
    exit;
  }
});

add_action( 'editable_roles' , function ( $roles ){
    if (is_not_developer()) {
      unset( $roles['developer'] );
      unset( $roles['application_submitter'] );
    }
    return $roles;
});

add_filter('edit_form_after_title', function() {
  $screen = get_current_screen();

  function post_meta($key, $multiple = false) {
    global $post;
    return get_post_meta($post->ID, $key, !$multiple);
  }

	if($screen->post_type == 'b4yp_applications' && $screen->id == 'b4yp_applications') {
    include 'application-admin-view.php';
  }

	if($screen->post_type == 'b4yp_locations' && $screen->id == 'b4yp_locations') {
    include 'location-admin-view.php';
  }

  if($screen->post_type == 'b4yp_votes' && $screen->id == 'b4yp_votes') {
    include 'vote-admin-view.php';
  }
});

add_filter('acf/load_field/key=field_6080c97d02580', function($field) {
    write_error($field);
    // return get_post_meta($post->ID, $key, true);
    return $field;
});

add_filter('views_edit-b4yp_locations', 'b4yp_add_filter_to_locations');
function b4yp_add_filter_to_locations($views) {
  $post_type = 'b4yp_locations';
  if( ( is_admin() ) && ( $_GET['post_type'] == $post_type ) ) {
      global $wp_query;

      $query = array(
        'post_type'   => $post_type,
        'post_status' => ['publish', 'draft'],
        'meta_query'  => [
          [
            'key' => 'year_awarded',
            'value' => date('Y'),
          ],
          [
            'key' => 'application_id',
            'compare' => 'EXISTS',
          ],
        ]
      );

      $attrs = isset($_GET['finalist']) ? ' class="current" aria-current="page"' : '';

      $result = new WP_Query($query);
      $views['finalists'] = sprintf(__('<a href="%s"'. $attrs .'>Finalist Leaderboard <span class="count">(%d)</span></a>'), admin_url("edit.php?post_type={$post_type}&finalist=true&orderby=count&order=asc"), $result->found_posts);

      return $views;
    }
}

function by4p_mime_types($mime_types){
  $mime_types['doc'] = 'application/msword';
  $mime_types['docx'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
  return $mime_types;
}
add_filter('upload_mimes', 'by4p_mime_types', 1, 1);

add_filter( 'manage_b4yp_locations_posts_columns', 'manage_b4yp_locations_columns' );
function manage_b4yp_locations_columns( $columns ) {
  $columns = array(
    'cb' => $columns['cb'],
    'title' => __( 'Title' ),
    'finalist' => __( 'Is Finalist?', 'b4yp' ),
    'count' => __( 'Vote Count', 'b4yp' ),
    'application' => __( 'Application', 'b4yp' ),
  );
  return $columns;
}

add_filter( 'manage_edit-b4yp_locations_sortable_columns', 'manage_b4yp_locations_sortable_columns');
function manage_b4yp_locations_sortable_columns( $columns ) {
  $columns['count'] = 'count';
  return $columns;
}

add_filter( 'manage_b4yp_votes_posts_columns', 'manage_b4yp_votes_columns' );
function manage_b4yp_votes_columns( $columns ) {
  $columns = array(
    'cb' => $columns['cb'],
    'title' => __( 'Title' ),
    'locations' => __( 'Locations voted for', 'b4yp' ),
  );
  return $columns;
}

add_action( 'manage_b4yp_locations_posts_custom_column', 'manage_b4yp_column_content', 10, 2);
add_action( 'manage_b4yp_votes_posts_custom_column', 'manage_b4yp_column_content', 10, 2);
function manage_b4yp_column_content( $column, $post_id ) {
  // Image column
  switch ($column) {
    case 'finalist' :
      $application_id = get_post_meta($post_id, 'application_id', true);
      echo get_post_meta($application_id, 'is_finalist', true) ? 'YES' : 'no';
      break;
    case 'count' :
      echo count(get_post_meta($post_id, 'vote_received'));
      break;
    case 'application' :
      $application_id = get_post_meta($post_id, 'application_id', true);
      if (!$application_id) break;
      echo "<a href='/wp-admin/post.php?post={$application_id}&action=edit' target='_blank'>View Application</a>";
      break;
    case 'locations' :
      $voted_for = get_post_meta($post_id, 'voted_for');
      $grouped = array_reduce($voted_for, function($groups, $location_id) {
        if (array_key_exists($location_id, $groups)) {
          $groups[$location_id]['count'] += 1;
        } else {
          $groups[$location_id] = [
            'url' => admin_url("post.php?post={$location_id}&action=edit"),
            'title' => get_the_title($location_id),
            'count' => 1
          ];
        }
        return $groups;
      }, []);
      foreach($grouped as $group) {
        echo "<strong>{$group['title']}</strong>: {$group['count']} - <a href='{$group['url']}' target='_blank'>View Location</a><br>";
      }
      break;
    default :
      echo '';
  }
}

function export_csv($data) {
  // No point in creating the export file on the file-system. We'll stream
  // it straight to the browser. Much nicer.

  // Open the output stream
  $fh = fopen('php://output', 'w');

  // Start output buffering (to capture stream contents)
  ob_start();
  // CSV Header
  $header = array('Location', 'Voter email', 'Number of votes', 'Location admin url', 'First voted at');
  fputcsv($fh, $header);

  // CSV Data
  foreach ($data as $row) {
    $line = array_values($row);
    fputcsv($fh, $line);
  }

  // Get the contents of the output buffer
  $string = ob_get_clean();

  // Set the filename of the download
  $filename = 'vote_data_' . date('Ymd') .'-' . date('His');

  // Output CSV-specific headers
  header('Pragma: public');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Cache-Control: private', false);
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename="' . $filename . '.csv";');
  header('Content-Transfer-Encoding: binary');

  // Stream the CSV data
  exit($string);
}

add_action('manage_posts_extra_tablenav', 'add_download_link_to_votes_index');
function add_download_link_to_votes_index($which) {
  global $post;

  if ($which === 'top' && $post->post_type === 'b4yp_votes') {
    echo '<a class="button" href="/wp-admin/edit.php?post_type=b4yp_votes&download=1" download>Download CSV</a>';
  }
}

function b4yp_posts_filter( $query ){
  global $pagenow;
  if (!isset($_GET['post_type'])) return;
  $type = $_GET['post_type'];
  if ( 'b4yp_locations' == $type && is_admin() && isset($_GET['finalist']) && $pagenow == 'edit.php') {
    $meta_query = array(
      'post_type'   => $post_type,
      'post_status' => ['publish', 'draft'],
      'meta_query'  => [
        [
          'key' => 'year_awarded',
          'value' => date('Y'),
        ],
        [
          'key' => 'application_id',
          'compare' => 'EXISTS',
        ],
      ]
    );
    $query->query_vars['meta_query'] = $meta_query; // add meta queries to $query
  }
}

add_filter( 'parse_query', 'b4yp_posts_filter' );

/**
 * Find the last period in the excerpt and remove everything after it.
 * If no period is found, just return the entire excerpt.
 *
 * @param string $excerpt The post excerpt.
 */
function end_with_sentence( $excerpt ) {
  $pos = strpos( $excerpt, '.' );
  if ($pos && $pos < 25) {
    $pos = strpos($excerpt, '.', $pos + 1);
  }
  if ( $pos !== false ) {
    $excerpt = substr( $excerpt, 0, $pos + 1 );
  } else {
    $excerpt = substr( $excerpt,  ) . '...';
  }
  return $excerpt;
}
add_filter( 'the_excerpt', 'end_with_sentence' );

function delete_votes_received($post_id) {
  $post_type = get_post_type($post_id);
  if ($post_type === 'b4yp_votes') {
    $args = [
      'post_type' => 'b4yp_locations',
      'post_per_page' => -1,
      'meta_key' => 'vote_received',
      'meta_value' => $post_id
    ];
    $posts = get_posts($args);
    foreach($posts as $post) {
      delete_post_meta($post->ID, 'vote_received', $post_id);
    }
  }
}

add_action( 'wp_trash_post', function($post_id) {
  // Verify if is trashing multiple posts
  if ( isset( $_GET['post'] ) && is_array( $_GET['post'] ) ) {
    foreach ( $_GET['post'] as $post_id ) {
      delete_votes_received( $post_id );
    }
  } else {
    delete_votes_received( $post_id );
  }
});
