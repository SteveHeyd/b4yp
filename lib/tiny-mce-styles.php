<?php

define( 'B4YP_CUSTOM_STYLES', [
  [
    'title' => 'Paragraphs',
    'items' => [
      [
        'title' => 'Standard',
        'attributes' => [ 'class' => '' ],
        'selector' => 'p',
      ],
      [
        'title' => 'Standouts',
        'items' => [
          [
            'title' => 'Teal',
            'attributes' => [ 'class' => 'standout standout--teal' ],
            'selector' => 'p',
          ],
          [
            'title' => 'Medium Teal',
            'attributes' => [ 'class' => 'standout standout--medium-teal' ],
            'selector' => 'p',
          ],
          [
            'title' => 'Dark Teal',
            'attributes' => [ 'class' => 'standout standout--dark-teal' ],
            'selector' => 'p',
          ],
          [
            'title' => 'Green',
            'attributes' => [ 'class' => 'standout standout--green' ],
            'selector' => 'p',
          ],
        ],
      ],
      [
        'title' => 'Footnote',
        'attributes' => [ 'class' => 'footnote' ],
        'selector' => 'p',
      ],
    ],
  ],
  [
    'title' => 'Headers',
    'items' => [
      [
        'title' => 'Heading 1',
        'classes' => 'heading-1',
        'block' => 'h3',
      ],
      [
        'title' => 'Heading 2',
        'classes' => 'heading-2',
        'block' => 'h3',
      ],
      [
        'title' => 'Heading 3',
        'classes' => 'heading-3',
        'block' => 'h3',
      ],
      [
        'title' => 'Header Accent',
        'inline' => 'small',
      ]
    ]
  ],
  [
    'title' => 'Vertical Spacing',
    'items' => [
      [
        'title' => 'Small',
        'items' => [
          [
            'title' => 'Bottom',
            'classes' => 'mb-8',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top',
            'classes' => 'mt-8',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top & Bottom',
            'classes' => 'my-8',
            'selector' => 'a,h3,p,em,strong'
          ],
        ]
        ],
      [
        'title' => 'Medium',
        'items' => [
          [
            'title' => 'Bottom',
            'classes' => 'mb-12',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top',
            'classes' => 'mt-12',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top & Bottom',
            'classes' => 'my-12',
            'selector' => 'a,h3,p,em,strong'
          ],
        ]
      ],
      [
        'title' => 'Large',
        'items' => [
          [
            'title' => 'Bottom',
            'classes' => 'mb-16',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top',
            'classes' => 'mt-16',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top & Bottom',
            'classes' => 'my-16',
            'selector' => 'a,h3,p,em,strong'
          ],
        ]
      ],
      [
        'title' => 'Extra Large',
        'items' => [
          [
            'title' => 'Bottom',
            'classes' => 'mb-24',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top',
            'classes' => 'mt-24',
            'selector' => 'a,h3,p,em,strong'
          ],
          [
            'title' => 'Top & Bottom',
            'classes' => 'my-24',
            'selector' => 'a,h3,p,em,strong'
          ],
        ]
      ],
    ]
  ],
  [
    'title' => 'Buttons',
    'items' => [
      [
        'title' => 'Primary',
        'classes' => 'button',
        'selector' => 'a',
      ],
      [
        'title' => 'Bordered',
        'classes' => 'button bordered',
        'selector' => 'a',
      ],
    ]
  ],
]);

function add_style_select_buttons( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );
//add custom styles to the WordPress editor
function my_custom_styles( $init_array ) {

  $style_formats = B4YP_CUSTOM_STYLES;
  // Insert the array, JSON ENCODED, into 'style_formats'
  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );

function b4yp_add_editor_styles() {
  add_editor_style( get_stylesheet_directory_uri() . '/dist/css/wysiwyg.css' );
}
add_action( 'admin_init', 'b4yp_add_editor_styles' );

if (function_exists('create_custom_style_select')) {
    create_custom_style_select('b4yp_styles', 'B4YP Styles', B4YP_CUSTOM_STYLES);
}

function my_toolbars( $toolbars ) {
	$toolbars['Very Simple'] = [ 1 => [ 'bold' , 'italic' ] ];
	$toolbars['Minimal'] = [
		1 => [
      'styleselect',
      'separator',
      'bold',
      'italic',
      'underline',
      'strikethrough',
      'separator',
      'link',
      'unlink',
      'separator',
      'undo',
      'redo',
      'alignleft',
      'aligncenter',
      'alignright',
      'alignjustify',
      'customInsertButton',
      'separator',
      'bullist',
      'numlist',
      'outdent',
      'indent',
    ]
	];
	return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars', 10, 1 );

function create_custom_style_select($id, $title, $styles)
{
    global $custom_style_select_styles;
    $custom_style_select_styles[] = array(
      'id' => $id,
      'title' => $title,
      'styles' => $styles
    );
}

function b4yp_format_TinyMCE( $in ) {
	$in['relative_urls'] = true;
	return $in;
}
add_filter( 'tiny_mce_before_init', 'b4yp_format_TinyMCE' );

function strip_from_permalink_in_results(&$item, $key, $strip) {
  $item['permalink'] = str_replace($strip, '', $item['permalink']);
}

function wplink_urls_to_relative($results) {
  $base_url = get_site_url();
  array_walk($results, 'strip_from_permalink_in_results', $base_url);
  return $results;
}
add_filter( 'wp_link_query', 'wplink_urls_to_relative', 10, 1 );

add_action('acf/input/admin_head', function()
{
    global $custom_style_select_styles, $custom_style_select_js_output;

    // Prevent duplicate output
    if ($custom_style_select_js_output)
        return;
    $custom_style_select_js_output = true;

    ?>
    <script type="text/javascript">

        var styles = <?=json_encode($custom_style_select_styles)?>;

        acf.add_filter('wysiwyg_tinymce_settings', function (settings) {
            var newFormats = [];
            var count = 0;

            settings.extended_valid_elements = 'button[type|class|aria-label|v-on:click|title]';

            var redefineFormats = [
              {
                name: 'alignleft',
                selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                classes: 'text-left'
              },
              {
                name: 'aligncenter',
                selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                classes: 'text-center'
              },
              {
                name: 'alignright',
                selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                classes: 'text-right'
              },
              {
                name: 'alignjustify',
                selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
                classes: 'text-justify'
              }
            ];

            function createMenu(items) {
                if (!items) {
                    return;
                }

                var menu = [];

                for (var i = 0, len = items.length; i < len; i++) {
                    var item = items[i];

                    var menuItem = {
                        text: item.title,
                        icon: item.icon
                    };

                    if (item.items) {
                        menuItem.menu = createMenu(item.items);
                    }
                    else {
                        var formatName = item.format || 'custom' + count++;

                        if (!item.format) {
                            item.name = formatName;
                            newFormats.push(item);
                        }

                        menuItem.format = formatName;
                        menuItem.cmd = item.cmd;
                    }

                    menu.push(menuItem);
                }

                return menu;
            }

            settings._oldSetup = settings.setup;
            settings.setup = function (editor) {
                if (settings._oldSetup && settings._oldSetup !== settings.setup)
                    settings._oldSetup(editor);

                editor.on('init', function () {
                    for (var i = 0, len = newFormats.length; i < len; i++) {
                        var format = newFormats[i];
                        editor.formatter.register(format.name, format);
                    }
                    for (var i = 0, len = redefineFormats.length; i < len; i++) {
                        var format = redefineFormats[i];
                        editor.formatter.unregister(format.name);
                        editor.formatter.register(format.name, format);
                    }
                });

                for (var i = 0, len = styles.length; i < len; i++) {
                    var style = styles[i];

                    editor.addButton(style.id, {
                        type: 'menubutton',
                        text: style.title,
                        menu: {
                            type: 'menu',
                            items: createMenu(style.styles),
                            onPostRender: function (e) {
                                editor.fire('renderFormatsMenu', {control: e.control});
                            },
                            itemDefaults: {
                                preview: true,

                                textStyle: function () {
                                    if (this.settings.format) {
                                        return editor.formatter.getCssText(this.settings.format);
                                    }
                                },

                                onPostRender: function () {
                                    var self = this;

                                    self.parent().on('show', function () {
                                        var formatName, command;

                                        formatName = self.settings.format;
                                        if (formatName) {
                                            self.disabled(!editor.formatter.canApply(formatName));
                                            self.active(editor.formatter.match(formatName));                                        }

                                        command = self.settings.cmd;
                                        if (command) {
                                            self.active(editor.queryCommandState(command));
                                        }
                                    });
                                },

                                onclick: function () {
                                    if (this.settings.format) {
                                        editor.execCommand('mceToggleFormat', false, this.settings.format);
                                    }

                                    if (this.settings.cmd) {
                                        editor.execCommand(this.settings.cmd);
                                    }
                                }
                            }
                        }
                    });
                }
            };

            return settings;
        });
    </script>
    <?php
});