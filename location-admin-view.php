<?php
global $post;
$application_id = post_meta('application_id');
$application_page = "/wp-admin/post.php?post={$application_id}&action=edit";
if ($application_id ) :
?>
<div class="location">
  <div class="application-details">
    <h3>Related Application</h3>
    <p><strong><?php echo get_the_title($application_id) ?></strong></p>
    <p><a href="<?php echo $application_page ?>" target="_blank">View Application</a></p>
  </div>
  <style type="text/css">
    .location .application-details {
      padding: 10px 20px;
      border: 1px solid black;
      border-radius: 10px;
      margin: 30px 0;
    }
  </style>
</div>
<?php
endif;
