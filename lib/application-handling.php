<?php

function update_application($application_id, $post, $updating) {
  // check if we intend this application to be a finalist
  if (get_field('finalist', $application_id)) {

    // exit if this application is already marked as a finalist
    if (
      get_post_meta($application_id, 'is_finalist', true) &&
      get_post_meta($application_id, 'location_id', true)
    ) return;

    // get application data
    $dog_park_type = get_post_meta($application_id, 'dogParkType', true);
    $community_name = get_post_meta($application_id, 'communityName', true);
    $park_name = get_post_meta($application_id, 'parkName', true);
    $address_array = get_post_meta($application_id, 'address', true);
    $deserve = get_post_meta($application_id, 'deserve', true);
    $images = get_post_meta($application_id, 'images', true);

    // create location post
    if ($dog_park_type == 'new') {
      $title = $community_name;
    } else {
      $title = $park_name;
    }
    $args = [
      'post_type' => 'b4yp_locations',
      'post_status' => 'draft',
      'post_title' => $title,
    ];
    $location_id = wp_insert_post($args);

    if (is_wp_error($location_id)) {
      $user_id = get_current_user_id();
      set_transient("location-saved-failed-{$application_id}-{$user_id}", $location_id, 3600);
      return;
    }

    // update metadata
    update_post_meta($location_id, 'application_id', $application_id);
    update_post_meta($application_id, 'is_finalist', true);
    update_post_meta($application_id, 'location_id', $location_id);

    // update ACF
    $year_awarded_key = 'field_605b9fd2d4004';
    update_field($year_awarded_key, 2021, $location_id);

    $gmap_field_key = "field_605b9f78d4003";
    $lat = $address_array['lat'];
    $lng = $address_array['lng'];
    unset($address_array['lat']); unset($address_array['lng']);
    $address = implode(', ', $address_array);
    $gmap_value = [ "address" => $address, "lat" => $lat, "lng" => $lng, "zoom" => 16 ];
    update_field($gmap_field_key, $gmap_value, $location_id);

    $enable_details_page_key = 'field_605e390b8ec5d';
    update_field($enable_details_page_key, true, $location_id);

    $image_ids = [];
    array_walk($images, function ($img) use ($location_id, &$image_ids) {
      $image_ids[] = $img['id'];
      $img['post_parent'] = $location_id;
      write_error($location_id);
      wp_update_post([ 'ID' => $img['id'], 'post_parent' => $location_id ]); // attach images to location post
    });
    $modules_field_key = 'field_604fada048531';
    $address_headline = $dog_park_type === 'existing' ? 'Location of park: ' : 'Location of proposed dog park: ';
    $street = $address_array['street'] !== '' ? $address_array['street'] . '<br>' : '';
    $modules = [
      [
        'acf_fc_layout' => 'content-band',
        'field_6050d09c4200e' => "<h4>{$address_headline}</h4> {$street} {$address_array['city']}, {$address_array['state']} {$address_array['zipcode']}",
        'field_60511348c8ece' => true,
      ],
      [
        'acf_fc_layout' => 'content-band',
        'field_6050d09c4200e' => "<h4>Why Our Community Deserves a <br>PetSafe® Bark for Your Park™ Grant Award:</h4> {$deserve}",
        'field_60511348c8ece' => true,
      ],
      [
        'acf_fc_layout' => 'content-band',
        'field_6050d09c4200e' => "<h4>Additional Photos:</h4>",
        'field_60511348c8ece' => true,
      ],
      [
        'acf_fc_layout' => 'gallery-band',
        'field_6084f1b69f3af' => $image_ids,
      ],
    ];
    update_field($modules_field_key, $modules, $location_id);
  }
}

add_action('save_post_b4yp_applications', 'update_application', 10, 3);

function display_application_save_errors($application_id) {
  $user_id = get_current_user_id();
  $transient_name = "location-saved-failed-{$application_id}-{$user_id}";
  if ( $error = get_transient($transient_name) ) :
    ?>
    <div class="error">
        <p><?php echo $error->get_error_message(); ?></p>
    </div>
    <?php
    delete_transient($transient_name);
  endif;
}
add_action( 'admin_notices', 'display_application_save_errors' );

function delete_location_post($location_id) {
  $application_id = get_post_meta($location_id, 'application_id', true);
  if ($application_id) {
    delete_post_meta($application_id, 'location_id');
    delete_post_meta($application_id, 'is_finalist');
  }
}
add_action( 'before_delete_post', 'delete_location_post', 10, 1);
