<?php
global $post;
$applicant_contact_details = [
  'firstName' => 'Fiest Name',
  'lastName' => 'Last Name',
  'email' => 'Email',
];
$prospective_park_location = [
  'communityName' => 'Community name:',
  'address' => 'Address:',
];
$additional_information = [
  'previousWinner' => 'Previous grant winner?',
  'population' => 'Community population:',
  'dogParkSize' => 'Size of the dog park:',
  'dogParkAccess' => 'Communities access to dog parks:',
  'additionalFunds' => 'Will be raising additional funds?',
  'additionalFundsHow' => 'How will additional funds be raised?',
  'maintenanceResponsibility' => 'Maintenance responsibility will fall to:',
  'maintenanceResponsibilityOtherExplanation' => 'Maintenance responsibility - explanation of "Other" selection:',
  'dedicatedSocialMedia' => 'Has a dedicated social media presence?',
  'sociaLinks' => 'Social Media Links',
  'deserve' => 'Why does this comunity deserve the grant?',
];
$acknowledged = [
  'readTermsAndConditions' => 'Read the terms and conditions?',
  'readCodeOfConduct' => 'Read the code of conduct?',
];
$files = [
  'civicLeaderForm' => 'Civic leader support form:',
  'landVerificationForm' => 'Land verification form:',
  'images' => 'Images:',
];
$location_id = post_meta('location_id');
$applicant_name = post_meta('firstName') . ' ' . post_meta('lastName');
$address = post_meta('address');
$lat = $address['lat'];
$lng = $address['lng'];
unset($address['lat']);
unset($address['lng']);
$isNewPark = post_meta('dogParkType') === 'New Park';
$email = post_meta('email');
$community_name = $isNewPark ? post_meta('parkName') : post_meta('communityName');
$civic_leader_form = post_meta('civicLeaderForm')[0];
$land_verification_form = post_meta('landVerificationForm')[0];
$filename_base = $post->ID . '_' . str_replace(' ', '-', $applicant_name) . '_' . str_replace(' ', '-', $community_name) . '_';
$images = post_meta('images');
?>
<div class="application">
  <?php
  if ($location_id) :
    $status = get_post_status($location_id);
    $location_page = get_edit_post_link($location_id);
  ?>
  <div class="location-details">
    <h3>Related Location Post</h3>
    <p><strong>Title: </strong> <?php echo get_the_title($location_id) ?></p>
    <p><strong>Post Status: </strong> <?php echo $status ?></p>
    <?php
    if ($status == 'trash') :
      $restore_link = wp_nonce_url(
        "post.php?action=untrash&amp;post=$location_id",
        "untrash-post_$location_id"
      );
    ?>
      <p><a href="<?php echo $restore_link ?>" target="_blank">Restore Location</a></p>
    <?php else : ?>
      <p><a href="<?php echo $location_page ?>" target="_blank">Edit Location</a></p>
    <?php endif; ?>
  </div>
  <?php endif ?>
  <h3>Application Contact</h3>
  <div class="meta-group">
    <div class="row">
      <p class="contact-details">
        <strong>Submitted By:</strong> <?php echo $applicant_name ?><br>
        <strong>Email:</strong> <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
      </p>
      <p class="acknowledgements">
        Acknowledged terms? <strong><?php echo strtoupper(post_meta('readTermsAndConditions')) ?></strong><br>
        Acknowledged code of conduct? <strong><?php echo strtoupper(post_meta('readCodeOfConduct')) ?></strong><br>
        Agreed to public use? <strong><?php echo strtoupper(post_meta('agreeToPublicUse')) ?></strong><br>
        <?php if ($isNewPark) : ?>
          Agreed to use Petsafe in park name? <strong><?php echo strtoupper(post_meta('agreeToNaming')) ?></strong><br>
        <?php endif ?>
      </p>
    </div>
    <p><strong>Park Type:</strong> <?php echo $isNewPark ? 'New Park' : 'Existing Park' ?></p>
    <p><strong><?php echo $isNewPark ? 'Park' : 'Community' ?> Name:</strong> <?php echo $community_name ?></p>
    <p><strong>Address:</strong> <?php echo implode(', ', $address) ?></p>
    <?php if ($lat && $lng) : ?>
      <div class="acf-map">
        <div class="marker" data-lat="<?php echo esc_attr($lat); ?>" data-lng="<?php echo esc_attr($lng); ?>">
          <h3><?php echo esc_html( $community_name ); ?></h3>
          <p><em><?php echo implode(', ', $address) ?></em></p>
        </div>
      </div>
    <?php else : ?>
      <p><em>Map is unavailable. Address failed to geocode for some reason</em></p>
    <?php endif ?>
    <?php foreach ($additional_information as $meta_key => $label) : ?>
      <?php
        $value = post_meta($meta_key);
        if ($value == '') $value = 'N/A or Not Provided';
        if (is_array($value)) :
          foreach ($value as $link) :
            if (filter_var($link, FILTER_FLAG_SCHEME_REQUIRED)) :
      ?>
        <p><a href="<?php echo $link ?>" target="_blank"><?php echo $link ?></a></p>
            <?php else : ?>
        <p><?php echo $link ?></p>
      <?php
            endif;
          endforeach;
        else :
      ?>
        <p><strong><?php echo $label ?></strong><br><?php echo $value ?></p>
      <?php endif ?>
    <?php endforeach ?>
    <p>
      <strong>Civic Leader Support Form:</strong>
      <button type="button" onclick="download(this)" data-filename="<?php echo $filename_base ?>civic-leader-form" data-href="<?php echo $civic_leader_form['source_url'] ?>">Download</button>
    </p>
    <p>
      <strong>Land Verification Form:</strong>
      <button type="button" onclick="download(this)" data-filename="<?php echo $filename_base ?>land-verification-form" data-href="<?php echo $land_verification_form['source_url'] ?>">Download</button>
    </p>
    <h4>Images</h4>
    <ul class="images">
      <?php foreach ($images as $image) : ?>
        <li>
          <a href="<?php echo $image['source_url'] ?>" target="_blank">
            <figure class="square-image">
              <img src="<?php echo $image['source_url'] ?>">
            </figure>
          </a>
        </li>
      <?php endforeach ?>
    </ul>
    <style type="text/css">
      .application .location-details {
        padding: 10px 20px;
        border: 1px solid black;
        border-radius: 10px;
        margin: 30px 0;
      }
      .application p {
        font-size: 16px;
      }
      .application .row{
        display: flex;
        flex-flow: row;
      }
      .application .acknowledgements {
        float: right;
        width: 50%;
      }
      .application .contact-details {
        float: left;
        width: 50%;
        clear: both;
      }
      .application .acf-map {
          width: 100%;
          height: 400px;
          border: #ccc solid 1px;
          margin: 20px 0;
      }

      .application .acf-map img {
        max-width: inherit !important;
      }
      .application .images {
        display: flex;
        flex-flow: row wrap;
      }
      .application .images li {
        width: 23%;
        padding: 1%;
      }
      .application .square-image {
        position: relative;
        width: 100%;
        display: block;
        margin: 0;
      }
      .application .square-image::after {
        content: "";
        display: block;
        padding-top: 100%;
      }
      .application .square-image img {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
      }
    </style>
    <script type="text/javascript">
      function download(elem) {
        var url = elem.getAttribute('data-href');
        var uri = new URL(url);
        var a = document.createElement('a');
        a.href = url.replace(uri.protocol, '');
        a.download = elem.getAttribute('data-filename');
        a.click();
      }
    </script>
    <?php if ($lat && $lng) : ?>
      <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GMAP_API_KEY ?>"></script>
      <script type="text/javascript">
        (function( $ ) {

        /**
         * initMap
         *
         * Renders a Google Map onto the selected jQuery element
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @return  object The map instance.
         */
        function initMap( $el ) {

            // Find marker elements within map.
            var $markers = $el.find('.marker');

            // Create gerenic map.
            var mapArgs = {
                zoom        : $el.data('zoom') || 16,
                mapTypeId   : google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map( $el[0], mapArgs );

            // Add markers.
            map.markers = [];
            $markers.each(function(){
                initMarker( $(this), map );
            });

            // Center map based on markers.
            centerMap( map );

            // Return map instance.
            return map;
        }

        /**
         * initMarker
         *
         * Creates a marker for the given jQuery element and map.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @param   object The map instance.
         * @return  object The marker instance.
         */
        function initMarker( $marker, map ) {

            // Get position from marker.
            var lat = $marker.data('lat');
            var lng = $marker.data('lng');
            var latLng = {
                lat: parseFloat( lat ),
                lng: parseFloat( lng )
            };

            // Create marker instance.
            var marker = new google.maps.Marker({
                position : latLng,
                map: map
            });

            // Append to reference for later use.
            map.markers.push( marker );

            // If marker contains HTML, add it to an infoWindow.
            if( $marker.html() ){

                // Create info window.
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });

                // Show info window when marker is clicked.
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open( map, marker );
                });
            }
        }

        /**
         * centerMap
         *
         * Centers the map showing all markers in view.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   object The map instance.
         * @return  void
         */
        function centerMap( map ) {

            // Create map boundaries from all map markers.
            var bounds = new google.maps.LatLngBounds();
            map.markers.forEach(function( marker ){
                bounds.extend({
                    lat: marker.position.lat(),
                    lng: marker.position.lng()
                });
            });

            // Case: Single marker.
            if( map.markers.length == 1 ){
                map.setCenter( bounds.getCenter() );

            // Case: Multiple markers.
            } else{
                map.fitBounds( bounds );
            }
        }

        // Render maps on page load.
        $(document).ready(function(){
            $('.acf-map').each(function(){
                var map = initMap( $(this) );
            });
        });

        })(jQuery);
      </script>
    <?php endif ?>
  </div>
</div>