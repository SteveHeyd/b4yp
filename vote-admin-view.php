<?php
global $post;
$voted_for = post_meta('voted_for', true);
$grouped = array_reduce($voted_for, function($groups, $location_id) {
  if (array_key_exists($location_id, $groups)) {
    $groups[$location_id]['count'] += 1;
  } else {
    $groups[$location_id] = [
      'url' => admin_url("post.php?post={$location_id}&action=edit"),
      'title' => get_the_title($location_id),
      'count' => 1
    ];
  }
  return $groups;
}, []);
$vote_not_allowed_count = count(post_meta('vote_not_allowed', true));
?>
<div class="location">
  <div class="application-details">
    <h3>Voted for the following parks</h3>
    <?php foreach($grouped as $location) : ?>
    <p>
      <a href="<?php echo $location['url'] ?>" target="_blank">
        <?php echo $location['title'] ?> - Voted <strong><?php echo $location['count'] ?></strong> time(s)
      </a>
    </p>
    <?php endforeach ?>
  </div>
  <style type="text/css">
    .location .application-details {
      padding: 10px 20px;
      border: 1px solid black;
      border-radius: 10px;
      margin: 30px 0;
    }

    .location p{
      font-size: 18px;
    }
  </style>
</div>
