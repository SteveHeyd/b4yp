<?php

class Bronto {
  protected $client;
  protected $contact;
  public $was_existing_contact = false;

  public function __construct() {
    $this->client = new SoapClient('https://api.bronto.com/v4?wsdl',
      array(
        'trace' => 1,
        'features' => SOAP_SINGLE_ELEMENT_ARRAYS
      )
    );

    $this->token = 'A2DB2EC3-E265-40FB-927E-399F076D6309';
  }

  public function login() {
    $sessionId = $this->client->login(array('apiToken' => $this->token))->return;

    $session_header = new SoapHeader("http://api.bronto.com/v4",
                                      'sessionHeader',
                                      array('sessionId' => $sessionId));

    $this->client->__setSoapHeaders(array($session_header));
  }

  public function add_contact($email) {
    $contacts = array('email' => $email);
    $write_result = $this->client->addOrUpdateContacts(array($contacts))->return;

    // The id returned will be used by the readContacts() call below.
    if (property_exists($write_result, 'errors')) {
      write_error("add_contact_error +++++++");
      write_error($write_result->results);
      throw new Exception("There was a problem submitting this email. Please try again.");
    } else {
      $this->was_existing_contact = $write_result->results[0]->isNew != true;
      $this->contact_id = $write_result->results[0]->id;
    }
  }

  private function get_contact() {
    $contactFilter = array(
      'id' => $this->contact_id
    );

    $contacts = $this->client->readContacts(
      array(
        'pageNumber' => 1,
        'includeLists' => true,
        'filter' => $contactFilter,
      )
    )->return;

    if($contacts[0]) {
      $this->contact = $contacts[0];
    } else {
      throw new Exception("There was an error. Please try again. Code 1001");
    }
  }

  public function add_contact_to_list($list_id) {
    $contactObject = array(
      "id" => $this->contact_id,
    );
    $mailListObject = array("id" => $list_id);

    $write_result = $this->client->addToList(array(
      'list' => $mailListObject,
      'contacts' =>$contactObject
    ))->return;

    // The id returned will be used by the readContacts() call below.
    if (property_exists($write_result, 'errors')) {
      write_error("add_contact_to_list_error +++++++");
      write_error($write_result->results);
      throw new Exception("There was an error. Please try again. Code 1002");
    } else {
      return true;
    }
  }
}
